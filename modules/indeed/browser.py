# -*- coding: utf-8 -*-
import base64
import os
import platform
import zlib
from datetime import date, datetime
import time

from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException

from woob.browser import URL, need_login
from woob.browser.selenium import SeleniumBrowser
from woob.exceptions import BrowserUnavailable, BrowserIncorrectPassword
from woob.tools.json import json
from woob.browser.impersonate import ImpersonateHTTPAdapter

from .pages.pages import SearchPage, IndexPage, PreLoginPage, AdvertPageAppliance
from .pages.apply.basic_apply_pages import BasicApplyPage
from .pages.apply.document_pages import DocumentPage
from .pages.apply.post_apply_pages import PostApplyPage
from .pages.apply.question_pages import QuestionPage
from .pages.apply.resume_page import ResumePage
from .pages.apply.review_apply_page import ReviewApplyPage
from .pages.apply.work_experience_page import WorkExperiencePage
from .pages.apply.intervention_page import InterventionPage

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class IndeedBrowser(SeleniumBrowser):
    BASEURL = 'https://fr.indeed.com'
    HEADLESS = True  # Always change to True for prod

    # This line will load a module to impersonate the tls fingerprint and bypass site protection See here for more
    # details https://gitlab.com/woob/woob/-/merge_requests/871?commit_id=422ff5bf7df07ebe2eb0c764e2bc43ed7cefa366
    # #6203403a366d337d843363c9530362b540d2e761
    HTTP_ADAPTER_CLASS = ImpersonateHTTPAdapter

    DRIVER = webdriver.Chrome
    WINDOW_SIZE = (1920, 1080)

    home = URL('/$', IndexPage)

    login_page = URL(r'https://secure.indeed.com/auth\?hl=fr_FR', PreLoginPage)
    account_page = URL(r'https://secure.indeed.com/settings/account', PreLoginPage)

    search_page = URL(r'https://fr.indeed.com/\?from=gnav-homepage', SearchPage)
    advert_page_appliance = URL(r'/viewjob\?jk=(?P<_id>.*)', AdvertPageAppliance)

    basic_apply_page = URL(r'https://smartapply.indeed.com/beta/indeedapply/form/contact-info', BasicApplyPage)
    resume_page = URL(r'https://smartapply.indeed.com/beta/indeedapply/form/resume', ResumePage)
    question_page = URL(r'https://smartapply.indeed.com/beta/indeedapply/form/questions', QuestionPage)
    work_experience_page = URL(r'https://smartapply.indeed.com/beta/indeedapply/form/work-experience',
                               WorkExperiencePage)
    review_page = URL(r'https://smartapply.indeed.com/beta/indeedapply/form/review', ReviewApplyPage)
    post_apply_page = URL(r'https://smartapply.indeed.com/beta/indeedapply/form/post-apply', PostApplyPage)
    document_page = URL(r'https://smartapply.indeed.com/beta/indeedapply/form/documents', DocumentPage)
    intervention_page = URL(r'https://smartapply.indeed.com/beta/indeedapply/form/intervention', InterventionPage)

    def __init__(self, storage, config, automated, *args, **kwargs):
        self.storage = storage
        self.config = config
        self.automated = automated
        self.logged = False
        self.identification_error = None
        super(IndeedBrowser, self).__init__(**kwargs)

    def get_user_quad(self):
        # This function will return a custom location depending on the name in the config file
        name_letter = self.config['name'].get().lower()[0] or "a"  # First letter of the name in lowercase
        last_name = self.config['last_name'].get().lower() or "nonymous"  # Last name in lowercase
        return 'user_data_' + name_letter + last_name

    def get_user_browser_dir(self):
        user_data_dir = self.get_user_quad()
        if platform.system() == "Windows":
            return os.path.join(os.getenv('USERPROFILE'), "AppData", "woob_cache", user_data_dir)
        elif platform.system() == "Linux":
            return os.path.join('/home', os.getlogin(), "woob_cache", user_data_dir)

    def _build_options(self, preferences):
        # Since the connexion cookies need to be correctly stored, the option user-data-dir is defined here

        options = super(IndeedBrowser, self)._build_options(preferences)
        #options.add_argument("--proxy-server=195.35.28.27:3128")
        # Options to avoid captcha detection
        options.add_argument("start-maximized")
        options.add_argument("disable-web-security")
        options.add_argument('--user-data-dir={}'.format(self.get_user_browser_dir()))
        #options.add_argument('--disable-blink-features=AutomationControlled')
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        options.arguments.extend(["--no-default-browser-check", "--no-first-run"])
        options.arguments.extend(["--no-sandbox", "--test-type"])

        options.add_experimental_option("useAutomationExtension", False)

        # A formal user agent is required to be blocked in headless mode
        user_agent = (
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36')
        options.add_argument(f'user-agent={user_agent}')
        return options

    def _load_cookies(self, cookie_state: str):
        try:
            uncompressed = zlib.decompress(base64.b64decode(cookie_state))
        except (TypeError, zlib.error, EOFError, ValueError):
            self.logger.error('Unable to uncompress cookies from storage')
            return

        try:
            jcookies = json.loads(uncompressed)
        except ValueError:
            self.logger.error('Unable to reload cookies from storage')
        else:
            for jcookie in jcookies:
                self.driver.add_cookie(jcookie)
            self.logger.debug('Reloaded cookies from storage')

    def go_home(self):
        self.home.stay_or_go()

    def set_cookie_consent(self):
        self.go_home()
        self.driver.add_cookie({
            "domain": ".indeed.com",
            "expirationDate": 99999999999,
            "httpOnly": False,
            "name": "OptanonAlertBoxClosed",
            "path": "/",
            "secure": False,
            "value": "{0}T{1}:49:37.046Z".format(date.today(), datetime.now().hour)
        })

    def check_login(self):
        try:
            self.go_home()
            self.driver.find_element_by_id('AccountMenu')
            self.logged = True
        except NoSuchElementException:
            self.logged = False

    def do_login(self, automated=False):
        self.set_cookie_consent()
        self.check_login()

        if self.logged:
            return

        if self.config['mail'].get() == '' or self.config['password'].get() == '':
            self.logger.exception("The login or the password is not correct")
            raise BrowserIncorrectPassword('Veuillez renseigner votre mail et votre mot de passe')

        self.login_page.go()

        try:
            self.page.pre_login(self.config['mail'].get())
            if self.page.detect_capcha():
                self.logger.error("There is a captcha in the login page after clicking continue.")
                self.logged = False
                self.identification_error = "login_page_captcha_error"
                return

            # OTP step (for the one that does not have a password
            if self.page.detect_otp_verification():
                self.logger.debug('An otp verification has been detected.')

                # Check now if there is some protection
                if self.page.detect_otp_verification_error:
                    self.logged = False
                    self.logger.debug('"There is a captcha in the otp verification page, aborting..')
                    self.identification_error = "otp_page_captcha_error"
                    return

                if not automated:
                    otp_passcode = input('Insérer le code de vérification recu par mail: ')
                    self.page.solve_otp_verification(otp_passcode)
                    self.page.continue_otp_verification()
                else:
                    self.config.woob.backends_config.edit_backend('indeed', {'otp': 'waiting'})

                    # Starting a loop to wait an otp with the backends file for max 5 minutes
                    index = 0
                    while index < 300:
                        if self.config['otp'].get() != "waiting":
                            self.page.solve_otp_verification(self.config['otp'].get())
                            self.page.continue_otp_verification()
                        else:
                            time.sleep(1)
                            index = index + 1

                    # Checking if the otp verification succeed or not
                    if self.check_login():
                        self.config.woob.backends_config.edit_backend('indeed', {'otp': 'none'})
                    else:
                        self.config.woob.backends_config.edit_backend('indeed', {'otp': 'required'})

            # Password step
            if self.page.detect_password_verification():
                self.logger.debug('The password step has been detected.')
                self.page.login(self.config['password'].get())

            # Check if there is any error message due to an incorrect password
            if self.page.detect_password_login_error():
                self.logger.debug('The password is incorrect')
                self.identification_error = 'incorrect_password'
                return

            # If the identification succeed, the page is the profile page
            if self.account_page.match(self.url):
                self.logger.debug("Success to identify the user.")
                self.logged = True
            else:
                self.logger.debug("Fail to identify the user for an unknown reason")
                self.logged = False
                self.identification_error = "unknown_error"
                return

        except TimeoutException as e:
            self.logger.exception("timeout while login.")
            self.identification_error = "browser_error"
            raise BrowserUnavailable(e.msg)

    @need_login
    def do_apply(self, _id):
        """
        Start to apply a job advert from an id
        """
        # Checking that the user is correctly logged
        if not self.logged:
            return "Impossible de postuler à cette offre, l'utilisateur n'est pas identifié :" + str(
                self.identification_error)

        # Go on the advert page if needed
        self.advert_page_appliance.stay_or_go(_id=_id)

        # Click on the apply button only if the appliance process is hosted on Indeed
        if not self.page.start_appliance():
            self.logger.debug('This job offer cannot be applied on Indeed.')
            return "Cette offre ne peut pas etre faite sur Indeed"
        else:
            # This step is required to make sure it will have no redirection (like if the user is not correctly logged)
            try:
                WebDriverWait(self.driver, 2).until(EC.url_changes(self.driver.current_url))
            except TimeoutException:
                pass

        # Since some steps might be non-existing, creating a simple switch statement that will exit only on success
        # or error
        appliance_process_completed = False
        while not appliance_process_completed:
            if self.basic_apply_page.match(self.url):
                self.basic_apply_page.stay_or_go()
                if self.page.is_here:
                    self.page.fill_basic_apply(self.config['name'].get(), self.config['last_name'].get(),
                                               self.config['place'].get(), self.config['phone_number'].get())

            elif self.resume_page.match(self.url):
                self.resume_page.stay_or_go()
                if self.page.is_here:
                    self.page.fill_resume_page(self.config['resume_path'].get())

            elif self.question_page.match(self.url):
                self.question_page.stay_or_go()
                if self.page.is_here:
                    question_page_output = self.page.fill_question_page(self.storage, self.get_user_quad(),
                                                                        self.automated)
                    if self.question_page.match(self.url):
                        return self.page.detect_error_message()
                    else:
                        if question_page_output == "questions":
                            return "Impossible de répondre à une ou plusieurs questions"
                        elif question_page_output == 'error':
                            return "Une ou plusieurs erreurs ont été détecté: " + question_page_output

            elif self.work_experience_page.match(self.url):
                self.work_experience_page.stay_or_go()
                if self.page.is_here:
                    self.page.fill_work_experience_page(self.config['work_experience_title'].get(),
                                                        self.config['work_experience_company'].get())
                else:
                    return "Unable to match the work experience page"

            elif self.document_page.match(self.url):
                self.document_page.stay_or_go()
                if self.page.is_here:
                    document_page_output = self.page.fill_document_page()
                    if document_page_output != 'success':
                        return document_page_output

                else:
                    return "Unable to match the document page"

            elif self.intervention_page.match(self.url):
                self.intervention_page.stay_or_go()
                if self.page.is_here:
                    self.page.fill_intervention_page()

            elif self.review_page.match(self.url):
                self.review_page.stay_or_go()
                if self.page.is_here:
                    if not self.page.fill_review_page():
                        return "Un captcha bloque la finalisation de l'envoi de candidature"

            elif self.post_apply_page.match(self.url):
                self.post_apply_page.stay_or_go()
                if self.page.is_here:
                    appliance_process_completed = True

            elif self.advert_page_appliance.match(self.url):
                self.advert_page_appliance.stay_or_go()
                if self.page.is_here:
                    if not self.page.start_appliance():
                        return

            elif self.driver.current_url.startswith('https://secure.indeed.com/auth?continue'):
                # This means that the user is not correctly identified
                self.refresh_connexion_cookies()

            else:
                return "Redirection vers l'url " + self.driver.current_url

            # Wait for another url to load
            try:
                WebDriverWait(self.driver, 2).until(EC.url_changes(self.url))
                while self.driver.execute_script('return document.readyState;') != 'complete':
                    self.driver.implicitly_wait(1)
                pass
            except TimeoutException:
                pass

        return "Succès du processus d'envoi de candidature pour l'offre " + _id

    def refresh_connexion_cookies(self):
        self.driver.delete_all_cookies()
        self.do_login()

    def search_job(self, metier='', contrat='', limit_date='', radius='', place=''):
        self.logger.debug('Start of the search_job.IndeedBrowser')
        self.search_page.go()
        return self.page.go_result_page(metier, contrat, limit_date, radius, place)

