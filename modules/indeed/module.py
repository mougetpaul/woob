# -*- coding: utf-8 -*-

# Copyright(C) 2013      Bezleputh
#
# This file is part of a woob module.
#
# This woob module is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This woob module is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this woob module. If not, see <http://www.gnu.org/licenses/>.

from collections import OrderedDict

from woob.tools.backend import Module, BackendConfig
from woob.capabilities.job import CapJob, BaseJobAdvert
from woob.tools.value import Value, ValueBackendPassword

from .browser import IndeedBrowser

__all__ = ['IndeedModule']


class IndeedModule(Module, CapJob):
    NAME = 'indeed'
    DESCRIPTION = u'indeed website for search and apply job offers'
    MAINTAINER = u'Auxilim_dev'
    EMAIL = 'mygoa.official@gmail.com'
    LICENSE = 'AGPLv3+'
    STORAGE = {"questions": {}}
    BROWSER = IndeedBrowser

    type_contrat_choices = OrderedDict([(k, u'%s' % (v)) for k, v in sorted({
                                                                                'all': u'Tous les emplois',
                                                                                'fulltime': u'Temps plein',
                                                                                'parttime': u'Temps partiel',
                                                                                'permanent': u'Durée indéterminée/CDI',
                                                                                'temporary': u'Intérim',
                                                                                'contract': u'Durée déterminée/CDD',
                                                                            }.items())])

    limit_date_choices = OrderedDict([(k, u'%s' % (v)) for k, v in sorted({
                                                                              'any': u'à tout moment',
                                                                              '15': u'depuis 15 jours',
                                                                              '7': u'depuis 7 jours',
                                                                              '3': u'depuis 3 jours',
                                                                              '1': u'depuis hier',
                                                                              'last': u'depuis ma dernière visite',
                                                                          }.items())])

    radius_choices = OrderedDict([(k, u'%s' % (v)) for k, v in sorted({
                                                                          '0': u'uniquement à cet endroit',
                                                                          '5': u'dans un rayon de 5 kilomètres',
                                                                          '10': u'dans un rayon de 10 kilomètres',
                                                                          '15': u'dans un rayon de 15 kilomètres',
                                                                          '25': u'dans un rayon de 25 kilomètres',
                                                                          '50': u'dans un rayon de 50 kilomètres',
                                                                          '100': u'dans un rayon de 100 kilomètres',
                                                                      }.items())])

    CONFIG = BackendConfig(Value('metier', label=u'Job name', masked=False, default=''),
                           Value('limit_date', label=u'Date limite', choices=limit_date_choices, default=''),
                           Value('contrat', label=u'Contract', choices=type_contrat_choices, default=''),
                           Value('place', label=u'Place', masked=False, default=''),
                           Value('radius', label=u'Radius', choices=radius_choices, default=''),
                           Value('otp', default='none', required=False),
                           Value('mail', label='Mail', default='', required=False),
                           ValueBackendPassword('password', label='Password', default='', required=False, masked=False),
                           Value('name', label='Prénom', default='', required=False),
                           Value('last_name', label='Nom', default='', required=False),
                           Value('phone_number', label='Phone number', default='', required=False),
                           Value('resume_path', label="Chemin d'accès CV", default='', required=False),
                           Value('work_experience_title', label='Titre expérience professionnelle', default='',
                                 required=False),
                           Value('work_experience_company', label='Entreprise expérience professionnelle', default='',
                                 required=False), )

    def search_job(self, pattern=None):
        return self.browser.search_job(metier=pattern)

    def advanced_search_job(self):
        return self.browser.search_job(metier=self.config['metier'].get(),
                                       limit_date=self.config['limit_date'].get(),
                                       contrat=self.config['contrat'].get(),
                                       place=self.config['place'].get(),
                                       radius=self.config['radius'].get())

    def get_job_advert(self, _id, advert=None):
        return self.browser.get_job_advert(_id, advert)

    def fill_obj(self, advert, fields):
        return self.get_job_advert(advert.id, advert)

    def create_default_browser(self):
        return self.create_browser(
            self.storage,
            self.config,
            True,
            self.config['mail'].get(),
            self.config['password'].get(),
            responses_dirname='./'
        )

    def apply_job_advert(self, _id, automated=False, storage=None):
        # The job offer id might be separated by ;
        """id_list = _id.split(';')
        output = ""
        for ele in id_list:
            output += self.browser.do_apply(ele, automated, storage)
        return output"""
        return self.browser.do_apply(_id)

    OBJECTS = {BaseJobAdvert: fill_obj}
