from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys

from woob.browser.pages import LoggedPage
from woob.browser.selenium import (
        SeleniumPage, VisibleXPath
)


class BasicApplyPage(LoggedPage, SeleniumPage):
    # This class group every function to complete the first step of appliance process with the basic info
    # The url must be https://smartapply.indeed.com/beta/indeedapply/form/contact-info

    is_here = VisibleXPath('//h1[text()[contains(.,"Indiquez vos coordonnées")]]')

    def custom_field_clear(self, element):
        # Since there is protection to correctly clear the field, a backspace stroke will be sent multiples time
        while element.get_attribute('value') != "":
            element.send_keys(Keys.BACKSPACE)

    def fill_basic_apply(self, name, last_name, city, phone_number):
        # For every field, the pre-filled value might be right
        # Since the value of the field is in the value attribute, the clear will be made with javascript instead of
        # clear() from selenium
        first_name_input = self.driver.find_element_by_id('input-firstName')
        if not first_name_input.get_attribute('value') == name:
            self.custom_field_clear(first_name_input)
            first_name_input.send_keys(name)

        last_name_input = self.driver.find_element_by_id('input-lastName')
        if not last_name_input.get_attribute('value') == last_name:
            self.custom_field_clear(last_name_input)
            last_name_input.send_keys(last_name)

        # The city might be optional
        try:
            city_input = self.driver.find_element_by_id('input-location.city')
            if not city_input.get_attribute('value') == city:
                self.custom_field_clear(city_input)
                city_input.send_keys(city)
        except NoSuchElementException:
            pass

        phone_input = self.driver.find_element_by_id('input-phoneNumber')
        if not phone_input.get_attribute('value') == phone_number:
            self.custom_field_clear(phone_input)
            phone_input.send_keys(phone_number)

        self.click_confirm_button()

    def click_confirm_button(self):
        self.browser.wait_xpath_clickable('//main/div/div/button')
        el = self.driver.find_element_by_xpath('//main/div/div/button')
        el.click()
