from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from woob.browser.pages import LoggedPage
from woob.browser.selenium import (
    SeleniumPage, VisibleXPath, AnyCondition
)


class DocumentPage(LoggedPage, SeleniumPage):
    # This class group every function to complete the second step of appliance process with the review
    # The url must be https://smartapply.indeed.com/beta/indeedapply/form/documents

    is_here = AnyCondition(
        VisibleXPath('//h1[text()[contains(.," pièces justificatives")]]'),
        VisibleXPath('//h1[text()[contains(.,"appuyer votre candidature")]]'),
        VisibleXPath('//h1[text()[contains(.,"Cet employeur demande une lettre de motivation pour cette candidature")]]')
    )
    #is_here = VisibleXPath('//h1[text()[contains(.,"Cet employeur demande une lettre de motivation pour cette candidature")]]')

    def fill_document_page(self):
        self.click_confirm_button()
        if self.detect_error_message():
            return self.driver.find_element_by_xpath('//span[contains(@data-testid, "CoverLetter-errorMsg")]').text
        else:
            return 'success'

    def detect_error_message(self):
        try:
            self.browser.wait_xpath_visible('//span[contains(@data-testid, "CoverLetter-errorMsg")]', timeout=3)
            return True
        except TimeoutException:
            return False

    def click_confirm_button(self):
        self.browser.wait_xpath_clickable('//button[contains(@type, "button")]', timeout=3)
        self.driver.find_element_by_xpath('//button[contains(@type, "button")]').click()

        # Adding some timeout because the button can take some times to load
        try:
            WebDriverWait(self.driver, 2).until(EC.url_changes(self.browser.url))
            while self.driver.execute_script('return document.readyState;') != 'complete':
                self.driver.implicitly_wait(1)
            pass
        except TimeoutException:
            pass



