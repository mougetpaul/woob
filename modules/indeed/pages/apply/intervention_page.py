
from woob.browser.pages import LoggedPage
from woob.browser.selenium import (
        SeleniumPage, VisibleXPath
)


class InterventionPage(LoggedPage, SeleniumPage):
    # This class group every function to complete the second step of appliance process with the review
    # The url must be https://smartapply.indeed.com/beta/indeedapply/form/documents

    is_here = VisibleXPath('//h1[text()[contains(.,"Cet employeur recherche le profil suivant")]]')

    def fill_intervention_page(self):
        self.click_confirm_button()

    def click_confirm_button(self):
        self.browser.wait_xpath_clickable('//button[text()[contains(.,"Continuer vers ma candidature")]]', timeout=5)
        self.driver.find_element_by_xpath('//button[text()[contains(.,"Continuer vers ma candidature")]]').click()
