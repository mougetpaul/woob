from woob.browser.pages import LoggedPage
from woob.browser.selenium import (
        SeleniumPage, VisibleXPath
)


class PostApplyPage(LoggedPage, SeleniumPage):
    # This class group every function to complete the second step of appliance process with the review
    # The url must be https://smartapply.indeed.com/beta/indeedapply/form/post-apply

    is_here = VisibleXPath('//h1[text()[contains(.,"Votre candidature a bien été envoyée.")]]')

