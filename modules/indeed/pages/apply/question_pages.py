from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from woob.browser.pages import LoggedPage
from woob.browser.selenium import (
    SeleniumPage, VisibleXPath
)


def format_answer_proposition(answer_proposition):
    # This function will format the answer_proposition to add ; between every element and return a string
    output = ""
    if answer_proposition != 'None':
        for ele in answer_proposition:
            output += ele + ";"
        return output
    else:
        return answer_proposition


class QuestionPage(LoggedPage, SeleniumPage):
    # This class group every function to complete the second step of appliance process with the question The url must
    # be https://smartapply.indeed.com/beta/indeedapply/form/questions/1 or
    # https://smartapply.indeed.com/beta/indeedapply/form/questions/2

    is_here = VisibleXPath('//h1[text()[contains(.,"Répondez à ces questions posées par l\'employeur")]]')

    def __init__(self, browser):
        super().__init__(browser)
        self.user_answer = None
        self.storage = None
        self.user_quad = None

    def custom_field_clear(self, element):
        # Since there is protection to correctly clear the field, a backspace stroke will be sent multiples time
        while element.get_attribute('value') != "":
            element.send_keys(Keys.BACKSPACE)

    def answer_generic_button_question(self):
        pass

    def answer_generic_checkbox_question(self):
        pass

    def answer_button_question(self, web_element):
        el_list = web_element.find_elements_by_xpath('.//fieldset/label')
        for el in el_list:
            if el.find_element_by_xpath('.//span').text == self.user_answer['user_answer']:
                el.find_element_by_xpath('.//input').click()
                return

    def answer_text_field_question(self, web_element):
        try:
            text_area = web_element.find_element_by_xpath('.//textarea')
            if text_area.text != self.user_answer['user_answer']:
                text_area.clear()
                text_area.send_keys(self.user_answer['user_answer'])
            return
        except NoSuchElementException:
            pass

        web_element.find_element_by_xpath('.//input').send_keys(self.user_answer['user_answer'])

    def answer_checkbox_question(self, web_element):
        # The user answer are separated with ;
        user_answer_list = self.user_answer['user_answer'].split(";")
        el_list = web_element.find_elements_by_xpath('.//fieldset/label')
        for el in el_list:
            checkbox_el = el.find_element_by_xpath('.//input')
            matching = False
            for user_answer in user_answer_list:
                if el.find_element_by_xpath('.//span').text == user_answer:
                    matching = True
                    if not checkbox_el.is_selected():
                        checkbox_el.click()

            # This option is to uncheck what need to be unchecked
            if not matching and checkbox_el.is_selected():
                checkbox_el.click()

    def answer_date_question(self, web_element):
        # This method will answer the question that require a date as an answer
        pass

    def answer_int_field_question(self, web_element):
        # This method will answer the question that require an int as an answer
        if web_element.find_element_by_xpath('.//input').get_attribute('value') != int(self.user_answer['user_answer']):
            self.custom_field_clear(web_element.find_element_by_xpath('.//input'))
            web_element.find_element_by_xpath('.//input').send_keys(int(self.user_answer['user_answer']))

    def answer_upload_question(self, web_element):
        # This method will answer the questions that required to upload a document, like a photo or degrees
        pass

    def answer_dropdown_question(self, web_element):
        select = Select(web_element.find_element_by_xpath('.//select'))
        select.select_by_visible_text(self.user_answer['user_answer'])
        pass

    def get_question_type(self, web_element_question):
        try:
            web_element_question.find_element_by_xpath(".//textarea")
            return 'text_field'
        except NoSuchElementException:
            pass

        try:
            web_element_question.find_element_by_xpath(".//fieldset")
            if web_element_question.get_attribute('role') == 'radiogroup':
                return 'button'
            elif web_element_question.get_attribute('role') == 'group':
                return 'checkbox'
        except NoSuchElementException:
            pass

        try:
            web_element_question.find_element_by_xpath(".//select")
            return 'dropdown_list'
        except NoSuchElementException:
            pass

        try:
            input_el = web_element_question.find_element_by_xpath(".//input")
            if input_el.get_attribute('type') == 'number':
                return 'int_field'
            elif input_el.get_attribute('type') == 'radio':
                return 'button'
            elif input_el.get_attribute('type') == 'date':
                return 'date'
            elif input_el.get_attribute('type') == 'checkbox':
                return 'checkbox'
            elif input_el.get_attribute('type') == 'text':
                return 'text_field'
        except NoSuchElementException:
            pass

        return 'unidentified'

    def get_question_text(self, web_element, question_type):
        """
        This function will return the text of the question
        """
        if question_type == 'text_field':
            return web_element.find_element_by_xpath('.//label/span/span/span').text
        elif question_type == 'button':
            return web_element.find_element_by_xpath('.//legend/div/span/span/span').text
        elif question_type == 'int_field':
            return web_element.text
        elif question_type == 'checkbox':
            return web_element.find_element_by_xpath('.//label/span/span/span').text
        elif question_type == 'dropdown_list':
            return web_element.find_element_by_xpath('.//label/span/span/span').text

    def get_question_web_element(self):
        # The web element are the global web element with a question and the possible answers
        # Every question is in a web element with the id 'q_0', 'q_1'
        question_web_element_list = []
        try:
            index = 0
            while True:
                el = self.driver.find_element_by_id('q_' + str(index))
                question_web_element_list.append(el)
                index += 1
        except NoSuchElementException:
            pass
        return question_web_element_list

    def get_answer_proposition(self, web_element, question_type):
        answer_proposition_list = []
        if question_type == 'text_field' or question_type == 'text_area' or question_type == 'int_field' or question_type == 'upload':
            return 'None'
        if question_type == 'button':
            el_list = web_element.find_elements_by_tag_name('label')
            for el in el_list:
                if el.text != "" and el.text is not None:
                    answer_proposition_list.append(el.text)

            if len(answer_proposition_list) == 0:
                el_list = web_element.find_elements_by_tag_name('input')
                for el in el_list:
                    if el.get_attribute('value') != "" and el.get_attribute('value') is not None:
                        answer_proposition_list.append(el.get_attribute('value'))

        elif question_type == 'checkbox':
            el_list = web_element.find_elements_by_xpath('.//fieldset/label/span')
            for el in el_list:
                if el.get_attribute('value') != "" and el.get_attribute('value') is not None:
                    answer_proposition_list.append(el.text)

            if len(answer_proposition_list) == 0:
                for el in el_list:
                    answer_proposition_list.append(el.text)

        elif question_type == 'dropdown_list':
            select = Select(web_element.find_element_by_xpath('.//select'))
            for propositions in select.options:
                if propositions.text != "" and propositions.text is not None:
                    answer_proposition_list.append(propositions.text)

        return answer_proposition_list

    def add_storage_question(self, question_type, question_text, answer_proposition, user_answer):
        # This function will add a question_text and the user_answer associated
        question = {
            'question_text': question_text,
            'user_answer': user_answer,
            'answer_proposition': format_answer_proposition(answer_proposition),
            'optional': 'false',
            'question_type': question_type
        }

        question_database = self.storage.get("questions", self.user_quad, default=False)
        index = 0
        keep_going = True
        while keep_going:
            if question_database:
                output = question_database.get('q_' + str(index))
                if output is None:
                    self.storage.set("questions", self.user_quad, "q_" + str(index), question)
                    self.storage.save()
                    self.logger.debug('success to add the question {} to the storage'.format(question_text))
                    keep_going = False
                else:
                    # Checking if the question is already in the database
                    if output.get('question_text') == question_text and output.get('answer_proposition') == format_answer_proposition(answer_proposition):
                        self.logger.debug('The question is already in job.storage')
                        keep_going = False
                    else:
                        index = index + 1
            else:
                self.storage.set("questions", self.user_quad, "q_" + str(index), question)
                self.storage.save()
                self.logger.debug('success to add the question {} to the storage'.format(question_text))
                keep_going = False

        return question

    def handle_empty_question(self, question_text, question_proposition, question_type):
        # This function will start actions to get the missing answer of the question
        # It depends on if the process is automated or not
        if len(question_proposition) == 0:
            return input('Répondez à la question: ' + question_text)
        else:
            output = question_text + '\n' + 'Choix possibles:' + '\n\n'
            if question_type == 'checkbox':
                output += 'Plusieurs choix possibles, séparez les par des ";" - Exemple: 1;2\n'
            index = 1
            for question in question_proposition:
                output += str(index) + ") [ ] " + question + '\n'
                index = index + 1

            answer = input(output)
            if question_type == 'checkbox' or question_type == 'dropdown_list':
                checkbox_answer_list = answer.split(";")
                final_answer = ""
                for ele in checkbox_answer_list:
                    final_answer += question_proposition[int(ele)-1]
                return final_answer
            else:
                return question_proposition[int(answer)-1]

    def retrieve_user_answer(self, question_text, answer_proposition):
        # The question and the user answers are stored as json
        """
        q_0:
            question_text: Avez-vous votre permis?
            user_answer: Oui
        """
        question_database = self.storage.get('questions', self.user_quad, default=False)
        index = 0
        keep_going = True
        while keep_going:
            try:
                if question_database:
                    question = question_database.get('q_' + str(index))
                    if question['question_text'] == question_text and question['answer_proposition'] == format_answer_proposition(answer_proposition):
                        return question
                    else:
                        index = index + 1
                else:
                    return None
            except Exception:
                keep_going = False

        return None

    def fill_question_page(self, storage, user_quad, automated):
        # This function will handle every question
        self.storage = storage
        self.user_quad = user_quad
        # If automated, every question that have no answer will be returned as json
        required_question = []

        # Get every question in the page
        web_element_question_list = self.get_question_web_element()

        for web_element in web_element_question_list:
            # Extract the required information
            question_type = self.get_question_type(web_element)
            question_text = self.get_question_text(web_element, question_type)
            answer_proposition = self.get_answer_proposition(web_element, question_type)

            # Retrieve the answer of the question is on the backend or ask directly
            self.user_answer = self.retrieve_user_answer(question_text, answer_proposition)

            # If the user answer is None it means the user did not answer it yet
            if self.user_answer is None:
                # Either the user is prompted to get the answer, or the apply process stop
                if automated:
                    question = self.add_storage_question(question_type, question_text, answer_proposition, "None")
                    required_question.append(question)
                else:
                    self.user_answer = {
                        'question_text': question_text,
                        'user_answer': self.handle_empty_question(question_text, answer_proposition, question_type)
                    }
                    self.add_storage_question(question_type, question_text, answer_proposition, self.user_answer['user_answer'])

            if self.user_answer is not None and self.user_answer['user_answer'] != 'None':
                # Start to answer the question depending on the type of the question
                if question_type == 'button':
                    self.answer_button_question(web_element)
                elif question_type == 'text_field':
                    self.answer_text_field_question(web_element)
                elif question_type == 'checkbox':
                    self.answer_checkbox_question(web_element)
                elif question_type == 'dropdown_list':
                    self.answer_dropdown_question(web_element)
                elif question_type == 'int_field':
                    self.answer_int_field_question(web_element)
                elif question_type == 'date':
                    self.answer_date_question(web_element)
                elif question_type == 'generic_button':
                    self.answer_generic_button_question()
                elif question_type == 'generic_checkbox':
                    self.answer_generic_checkbox_question()
                elif question_type == 'upload':
                    self.answer_upload_question(web_element)

        if len(required_question) > 0 or self.user_answer['user_answer'] == 'None':
            self.logger.debug("Voici combien de question ont été ajouté: " + str(len(required_question)))
            return "questions"
        else:
            return self.click_confirm_button()

    def detect_error_message(self):
        # An error message can exist in every question if the question is not filled
        web_element_question_list = self.get_question_web_element()
        for web_element_question in web_element_question_list:
            try:
                web_element_question.find_element_by_xpath('.//div/div/div/div/div').text
                question_type = self.get_question_type(web_element_question)
                question_text = self.get_question_text(web_element_question, question_type)
                return "Problème avec la question: {} ".format(question_text)
            except NoSuchElementException:
                pass

        return "Impossible de trouver le message d'erreur"

    def click_confirm_button(self):
        self.browser.wait_xpath_clickable('//div[contains(@class, "ia-BasePage-footer dd-privacy-allow")]/div/button')
        self.driver.find_element_by_xpath(
            '//div[contains(@class, "ia-BasePage-footer dd-privacy-allow")]/div/button').click()

        # Adding some timeout because the button can take some times to load
        try:
            WebDriverWait(self.driver, 5).until(EC.url_changes(self.browser.url))
            while self.driver.execute_script('return document.readyState;') != 'complete':
                self.driver.implicitly_wait(1)
            pass
        except TimeoutException:
            pass

        return "success"
