import os

from selenium.common.exceptions import TimeoutException

from woob.browser import APIBrowser
from woob.browser.filters.standard import (
    CleanText
)
from woob.browser.selenium import (
        SeleniumPage, VisibleXPath
)


class ResumePage(SeleniumPage, APIBrowser):
    # This class group every function to complete the second step of appliance process with the resume
    # The url must be https://smartapply.indeed.com/beta/indeedapply/form/resume

    is_here = VisibleXPath('//h1[text()[contains(.,"Ajoutez un CV pour l\'employeur")]]')

    def fill_resume_page(self, resume_path):
        # This function will start every step require to complete the resume step
        if self.get_preloaded_resume_name() == os.path.basename(resume_path):
            if not self.check_resume_upload():
                self.enable_preloaded_resume()
        else:
            self.change_resume(resume_path)

        self.click_confirm_button()
        error_message = self.get_error_message()
        if error_message:
            return error_message
        else:
            return 'success'

    def check_resume_upload(self):
        # This function will check if the resume is already uploaded or not
        try:
            self.browser.wait_xpath_visible('//canvas[contains(@class, "react-pdf__Page__canvas")]', timeout=2)
            return True
        except TimeoutException:
            return False

    def get_preloaded_resume_name(self):
        # This function will retrieve the name of the preloaded resume to check if it's the right one
        return self.driver.find_element_by_xpath('//span[@data-testid="FileResumeCardHeader-title"]/span').text

    def enable_preloaded_resume(self):
        # This function will click on the preloaded resume to be able to go to the next step
        self.driver.find_element_by_xpath('//div[@data-testid="FileResumeCard"]').click()

    def change_resume(self, resume_path):
        # This method will send a request to change the resume
        el = self.driver.find_element_by_xpath('//input[contains(@data-testid, "FileResumeCard-file-input")]')
        el.send_keys(resume_path)

        self.browser.wait_xpath_clickable('//button[contains(@data-testid, "ResumePrivacyModal-SaveBtn")]')
        self.driver.find_element_by_xpath('//button[contains(@data-testid, "ResumePrivacyModal-SaveBtn")]').click()

    def click_confirm_button(self):
        self.browser.wait_xpath_visible('//div[contains(@class, "ia-BasePage-footer dd-privacy-allow")]')
        self.driver.find_element_by_xpath('//div[contains(@class, "ia-BasePage-footer dd-privacy-allow")]').click()

    def get_error_message(self):
        try:
            return CleanText('//main/div[1]/div/div/span[contains(@role, "alert")]')
        except TimeoutError:
            pass
