from selenium.common.exceptions import NoSuchElementException

from woob.browser.pages import LoggedPage
from woob.browser.selenium import (
        SeleniumPage, VisibleXPath
)


class ReviewApplyPage(LoggedPage, SeleniumPage):
    # This class group every function to complete the second step of appliance process with the review
    # The url must be https://smartapply.indeed.com/beta/indeedapply/form/review

    is_here = VisibleXPath('//h1[text()[contains(.,"Passez en revue votre candidature")]]')

    def fill_review_page(self):
        return self.click_confirm_button()

    def detect_final_captcha(self):
        # Indeed usually set a captcha just before the end of the appliance process
        try:
            self.driver.find_element_by_id("rc-anchor-container")
            return True
        except NoSuchElementException:
            return False

    def click_confirm_button(self):
        if not self.detect_final_captcha():
            self.browser.wait_xpath_clickable('//div[contains(@class, "ia-BasePage-footer dd-privacy-allow")]/div/button')
            self.driver.find_element_by_xpath('//div[contains(@class, "ia-BasePage-footer dd-privacy-allow")]/div/button').click()
            return True
        else:
            return False
