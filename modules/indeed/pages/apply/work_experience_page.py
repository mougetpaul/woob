from selenium.webdriver.common.keys import Keys

from woob.browser.pages import LoggedPage
from woob.browser.selenium import (
        SeleniumPage, VisibleXPath
)


class WorkExperiencePage(LoggedPage, SeleniumPage):
    # This class group every function to complete the second step of appliance process with the work experience
    # The url must be https://smartapply.indeed.com/beta/indeedapply/form/work-experience

    is_here = VisibleXPath('//h1[text()[contains(.,"indiquer votre expérience")]]')

    def fill_work_experience_job_title(self, work_experience_job_title):
        self.driver.find_element_by_id('jobTitle').send_keys(work_experience_job_title)

    def fill_work_experience_job_company(self, work_experience_company):
        # Since there is some autocompletion company, it might cause click interception for the next button
        self.driver.find_element_by_id('companyName').send_keys(work_experience_company)
        self.driver.find_element_by_id('companyName').send_keys(Keys.TAB)

    def fill_work_experience_page(self, work_experience_job_title, work_experience_company):
        self.fill_work_experience_job_title(work_experience_job_title)
        self.fill_work_experience_job_company(work_experience_company)
        self.click_confirm_button()

    def click_confirm_button(self):
        self.browser.wait_xpath_clickable('//div[contains(@class, "ia-BasePage-footer dd-privacy-allow")]/div/button')
        self.driver.find_element_by_xpath('//div[contains(@class, "ia-BasePage-footer dd-privacy-allow")]/div/button').click()


