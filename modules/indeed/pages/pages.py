# -*- coding: utf-8 -*-
import time
import traceback
# Copyright(C) 2013      Bezleputh
#
# This file is part of a woob module.
#
# This woob module is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This woob module is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this woob module. If not, see <http://www.gnu.org/licenses/>.

from datetime import timedelta, datetime
import re

from selenium.common.exceptions import TimeoutException, ElementClickInterceptedException
from selenium.webdriver.common.keys import Keys
from woob.browser.pages import HTMLPage, LoggedPage, JsonPage
from woob.browser.elements import ListElement, ItemElement, method
from woob.browser.filters.standard import Filter, CleanText, Format, Env
from woob.browser.filters.html import Attr
from woob.browser.selenium import SeleniumPage, VisibleXPath
from woob.capabilities.job import BaseJobAdvert
from woob.capabilities.base import NotAvailable


class IndexPage(SeleniumPage):
    pass


class IndeedUrl(Filter):
    def filter(self, url):
        return "https://fr.indeed.com/viewjob?jk={}".format(url)


class IndeedDate(Filter):
    def filter(self, date):
        now = datetime.now()
        number = re.search("\d+", date)
        if number:
            if 'heures' in date:
                return now - timedelta(hours=int(number.group(0)))
            elif 'jour' in date:
                return now - timedelta(days=int(number.group(0)))
        return now


class IndeedCompany(Filter):
    # This class need to filter the css object after the company name
    def filter(self, company):
        css_index = re.search(".css", company)
        return company[0:css_index.start()]


class SearchPage(SeleniumPage):

    # is here will check if the button "Rechercher" is visible
    is_here = VisibleXPath('//button[@type="submit"]')

    def custom_field_clear(self, element):
        # Since there is protection to correctly clear the field, a backspace stroke will be sent multiples time
        while element.get_attribute('value') != "":
            element.send_keys(Keys.BACKSPACE)

    def detect_captcha(self):
        try:
            if self.driver.find_element_by_xpath('//main[@class="error"]/h1').text == "Additional Verification Required":
                return True
            else:
                return False
        except Exception:
            return False

    def insert_title_job(self, job_title):
        if self.driver.find_element_by_xpath('//input[@id="text-input-what"]').get_attribute('value') != job_title:
            self.custom_field_clear(self.driver.find_element_by_xpath('//input[@id="text-input-what"]'))
            self.driver.find_element_by_xpath('//input[@id="text-input-what"]').send_keys(job_title)

    def insert_place_job(self, place):
        if self.driver.find_element_by_xpath('//input[@id="text-input-where"]').get_attribute('value') != place:
            self.custom_field_clear(self.driver.find_element_by_xpath('//input[@id="text-input-where"]'))
            self.driver.find_element_by_xpath('//input[@id="text-input-where"]').send_keys(place)

    def start_result_page(self):
        if self.driver.find_element_by_xpath('//button[@type="submit"]').text == "Rechercher":
            self.driver.find_element_by_xpath('//button[@type="submit"]').click()

    def insert_radius_options(self, radius):
        self.open_radius_options()
        self.get_radius_web_element(radius)

    def open_radius_options(self):
        self.browser.wait_xpath_clickable('//button[@id="filter-radius"]')
        self.driver.find_element_by_xpath('//button[@id="filter-radius"]').click()

    def get_radius_web_element(self, radius):
        self.browser.wait_xpath_clickable('//form[@id="filter-radius"]/fieldset/label')
        contrat_type_list_web_element = self.driver.find_elements_by_xpath('//form[@id="filter-radius"]/div')
        for web_element in contrat_type_list_web_element:
            if radius in web_element.text:
                web_element.click()

    def open_contrat_options(self):
        self.browser.wait_xpath_clickable('//button[@id="filter-jobtype1"]')
        self.driver.find_element_by_xpath('//button[@id="filter-jobtype1"]').click()

    def get_contrat_web_element(self, contrat):
        self.browser.wait_xpath_clickable('//form[@id="filter-jobtype1-menu"]/fieldset/label')
        contrat_type_list_web_element = self.driver.find_elements_by_xpath('//form[@id="filter-jobtype1-menu"]/fieldset/label')
        for web_element in contrat_type_list_web_element:
            if web_element.text == contrat:
                web_element.click()

    def confirm_contrat_options(self):
        self.browser.wait_xpath_clickable('//button[@form="filter-jobtype1-menu"]')
        self.driver.find_element_by_xpath('//button[@form="filter-jobtype1-menu"]').click()

    def insert_contrat_options(self, contrat):
        self.open_contrat_options()
        self.get_contrat_web_element(contrat)

    def insert_advanced_options(self, contrat='', radius=''):
        if contrat != "" and contrat != "all":
            self.insert_contrat_options(contrat)

        if radius != "25":
            self.insert_radius_options(radius)

    def go_result_page(self, job_title='', contrat='', limit_date='', radius='', place=''):
        try:
            self.insert_title_job(job_title)
            self.insert_place_job(place)
            self.start_result_page()
            self.insert_advanced_options(contrat, radius)
            return self.iter_job_adverts()
        except Exception:
            if self.detect_captcha():
                self.logger.debug("Un captcha bloque les résultats de recherche: ")
                return "Un captcha bloque les résultats de recherche"
            else:
                self.logger.debug("Une erreur est survenue pendant la recherche: " + traceback.format_exc())
                self.logger.debug("Code source de la page: " + self.driver.page_source)

    @method
    class iter_job_adverts(ListElement):

        item_xpath = '//div[@id="mosaic-jobResults"]/div/ul/li/div/div/div/div/div/div'

        class Item(ItemElement):
            klass = BaseJobAdvert
            obj_id = Attr('./table[1]/tbody/tr/td/div/h2/a', 'data-jk', default=NotAvailable)
            obj_url = IndeedUrl(Attr('./table[1]/tbody/tr/td/div/h2/a', 'data-jk', default=NotAvailable))
            obj_job_name = CleanText('./table[1]/tbody/tr/td/div/h2')
            obj_title = CleanText('./table[1]/tbody/tr/td/div/h2')
            obj_society_name = CleanText('./table[1]/tbody/tr/td/div/div/div/span[1]')
            obj_place = CleanText('./table[1]/tbody/tr/td/div/div/div[@data-testid="text-location"]')
            obj_publication_date = IndeedDate(CleanText('./div/div/div/span/span'))
            obj_pay = CleanText('./table[1]/tbody/tr/td/div[3]/ul/li/div/div')
            obj_description = CleanText('./div[@role="presentation"]/div[2]/div/div[@data-testid="jobsnippet_footer"]')


class AdvertPage(HTMLPage):
    @method
    class get_job_advert(ItemElement):
        klass = BaseJobAdvert

        def parse(self, el):
            self.env['url'] = self.page.url
            self.env['num_id'] = self.page.url.split('-')[-1]

        obj_id = Format('%s#%s#%s',
                        Env('num_id'),
                        CleanText('//div/h1/span'),
                        CleanText(
                            '//body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[1]/div[2]/div/div/div/div[1]/div/span'),
                        )
        obj_title = CleanText('//div/h1/span')
        obj_job_name = CleanText('//div/h1/span')
        obj_place = CleanText('//div[@id="jobLocationText"]/div/span')
        obj_society_name = IndeedCompany(CleanText('//div[@data-testid="inlineHeader-companyName"]/span/a'))
        obj_description = CleanText('//div[@id="jobDescriptionText"]')
        obj_contract_type = CleanText('//div[@id="salaryInfoAndJobType"]/span[2]', replace=[(u'- ', '')])
        obj_pay = CleanText('//div[@id="salaryInfoAndJobType"]/span[1]')
        obj_url = Env('url')
        obj_publication_date = IndeedDate(CleanText('//span[@class="date"]'))


class VerifyPage(JsonPage):
    pass


class PreLoginPage(SeleniumPage):

    @property
    def url(self):
        return self.browser.url

    def pre_login(self, username):
        self.driver.find_element_by_xpath('//input[@name="__email"]').send_keys(username)
        time.sleep(2)
        self.driver.find_element_by_xpath('//button[@data-tn-element="auth-page-email-submit-button"]').click()

    def detect_captcha(self):
        try:
            self.browser.wait_xpath_visible('//div[@data-tn-element="auth-page-email-captcha-captcha-puzzle-widget"]', timeout=5)
            return True
        except TimeoutException:
            return False

    def login(self, password):
        self.browser.wait_xpath_visible('//input[@name="__password"]')
        el = self.driver.find_element_by_xpath('//input[@name="__password"]')
        el.send_keys(password)
        el = self.driver.find_element_by_xpath(
            '//button[@data-tn-element="auth-page-sign-in-password-form-submit-button"]')
        el.click()

    def check_login_status(self):
        # This function will check if the user is connected
        try:
            self.browser.wait_xpath_visible('//button[@data-gnav-element-name=="BurgerMenu"]', timeout=3)
            return True
        except TimeoutException:
            return False

    def detect_otp_verification(self):
        try:
            self.browser.wait_xpath_visible('//h1[@data-testid="auth-page-otp-header"]', timeout=3)
            return True
        except TimeoutException:
            return False

    def detect_otp_verification_error(self):
        try:
            self.browser.wait_xpath_visible('//div[@id="label-passcode-input-error"]', timeout=3)
            return True
        except TimeoutException:
            return False

    def detect_otp_verification_error_message(self):
        try:
            self.browser.wait_xpath_visible('//h1[@data-testid="auth-page-otp-header"]', timeout=3)
            return True
        except TimeoutException:
            return False

    def solve_otp_verification(self, otp_passcode):
        el = self.driver.find_element_by_id('passcode-input')
        el.send_keys(otp_passcode)

    def continue_otp_verification(self):
        self.browser.wait_xpath_visible('//button[@data-tn-element="otp-verify-login-submit-button"]', timeout=1)
        self.driver.find_element_by_xpath('//button[@data-tn-element="otp-verify-login-submit-button"]').click()

    def detect_password_verification(self):
        try:
            self.browser.wait_xpath_visible('//input[@name="__password"]', timeout=3)
            return True
        except TimeoutException:
            return False

    def detect_password_login_error(self):
        try:
            self.driver.find_element_by_id('ifl-InputFormField-errorTextId-ihl-useId-passport-webapp-3')
            return True
        except TimeoutException:
            return False


class AdvertPageAppliance(SeleniumPage, LoggedPage):

    def start_appliance(self):
        # Since some job offer redirect to an external website, this button can be clicked only if the job offer is
        # hosted on indeed. The button must have only the text 'Postuler' inside and the id must be indeedApplyButton
        try:
            self.browser.wait_xpath_clickable('//button[@aria-label="Postuler maintenant "]', timeout=5)
            self.driver.find_element_by_xpath('//button[@aria-label="Postuler maintenant "]').click()
            return True
        except TimeoutException:
            return False
        except ElementClickInterceptedException as e:
            # This error means that there is a cookie accept banner
            self.logger.error("An error occurred during clicking on the apply button: " + e.msg)
