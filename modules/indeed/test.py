# -*- coding: utf-8 -*-
# Copyright(C) 2013      Bezleputh
#
# This file is part of a woob module.
#
# This woob module is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This woob module is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this woob module. If not, see <http://www.gnu.org/licenses/>.


from woob.tools.test import BackendTest
from woob.tools.value import Value


class IndeedTest(BackendTest):
    MODULE = 'indeed'
    job_offer_list = {}

    def setUp(self):
        if not self.is_backend_configured():
            self.backend.config['limit_date'] = Value(value='any')
            self.backend.config['metier'] = Value(value='informaticien')
            self.backend.config['contrat'] = Value(value='contract')

    def test_indeed_search(self):
        for ele in ['informaticien', 'livreur', 'boulanger']:
            self.job_offer_list = list(self.backend.search_job(ele))
            assert len(self.job_offer_list)

    def test_indeed_advanced_search(self):
        self.job_offer_list = list(self.backend.advanced_search_job())
        assert len(self.job_offer_list)

    def test_indeed_info_from_id(self):
        if len(self.job_offer_list) == 0:
            self.job_offer_list = list(self.backend.search_job('informaticien'))
            assert len(self.job_offer_list) > 0

        advert = self.backend.get_job_advert(self.job_offer_list[0].id)
        self.assertTrue(advert.url, 'URL for announce "%s" not found: %s' % (advert.id, advert.url))

    # Since it require resume and question answer, there is no unit test for apply indeed..
    """def test_apply_job_offer(self):
        self.setUp()
        if len(self.job_offer_list) == 0:
            self.job_offer_list = list(self.backend.advanced_search_job())
            assert len(self.job_offer_list)
        try:
            output = self.backend.apply_job_advert(self.job_offer_list[0].id)
            # output = self.backend.apply_job_advert('9945cc642b28b1ed')
            assert output.startswith("Fin du processus") or output.startswith("Impossible")
        except SentOTPQuestion:
            self.backend.config['otp_email'].set(input('Give me your OTP baby'))
            _ = self.backend.apply_job_advert('b7a06fab3cd78dc9')"""
