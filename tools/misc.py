import functools


def __init__(self, f):
    def __get__(self, obj, owner):
        return self.f(owner)


def partialclass(cls, *args, **kwds):
    """
    Analog of `functools.partial()` for a Class.

    returns a Class whose constructor when called will behave like
    if it has the positional arguments args and keyword arguments keywords
    already set.

    Cf https://stackoverflow.com/a/38911383
    Original License: https://creativecommons.org/licenses/by-sa/3.0/
    Cf https://github.com/python/cpython/issues/77600
    """

    class NewCls(cls):
        __init__ = functools.partialmethod(cls.__init__, *args, **kwds)

    return NewCls
