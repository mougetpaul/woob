# Copyright(C) 2023      Ludovic LANGE
#
# This file is part of woob.
#
# woob is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# woob is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with woob. If not, see <http://www.gnu.org/licenses/>.
"""
This module provides an http client adapter, based on curl, that can
impersonate a real browser tls/ja3/http2 fingerprint.

To use it, add :class:`ImpersonateHTTPAdapter` as an :class:`HTTP_ADAPTER_CLASS`
in your module's browser class.

An optional constructor parameter is possible - `impersonate_as` - to choose a version
of browser you want to simulate.
It must be from the [list of supported browsers](https://github.com/lwthiker/curl-impersonate/blob/main/browsers.json)
in your curl-impersonate backend version.
(Please note that Firefox needs a different curl-impersonate binary than
IE/Safari/Chrome as they use a different TLS backend)

To use this `impersonate_as` constructor parameter, you'll need the following construct:
```
from woob.browser.impersonate import ImpersonateHTTPAdapter
from woob.tools.misc import partialclass
...

class MyModuleBrowser(LoginBrowser):
    HTTP_ADAPTER_CLASS = partialclass(ImpersonateHTTPAdapter, impersonate_as='chrome107')
...
```


By default, it'll try to use one python interface to curl-impersonate among:
- requests_curl     https://github.com/paivett/requests-curl
- curl_cffi_adapter (not public - yet)
- pycurl_requests   https://github.com/dcoles/pycurl-requests

You can force one of these by setting the env variable WANTED_CURL_ADAPTER to one
of the strings "requests_curl", "curl_cffi", or "pycurl_requests"
(otherwise it'll load the first available.)

Main differences between adapters:
* requests_curl : works OK, but no debug traces
  * https://github.com/paivett/requests-curl (last commit from 2021 / not in PyPi)
  * uses pycurl backend, for which last cURL version is 7.45.2
* curl_cffi_adapter : works OK, but no debug traces
  * a very recent fork of requests_curl adapted to curl_cffi backend.
  * uses curl_cffi backend, for which last cURL version is 7.84.0
  * not public (yet), just ask.
* pycurl_requests : fails with "necessary data rewind wasn't possible (cURL error: 65)"
  * HAR (save-response) doesn't work
  * debug traces are nice and complete
  * uses pycurl backend, for which last cURL version is 7.45.2
  * https://github.com/dcoles/pycurl-requests (last commit from Jan 2023)
"""

from os import environ
from urllib3.util import SKIP_HEADER
import logging

LOGGER = logging.getLogger(__name__)
DEFAULT_BROWSER_SIGNATURE = "chrome110"

WANTED_CURL_ADAPTER = environ.get("WANTED_CURL_ADAPTER", None)
if WANTED_CURL_ADAPTER and WANTED_CURL_ADAPTER not in [
    "requests_curl",
    "curl_cffi",
    "pycurl_requests",
]:
    raise ImportError(
        "Env variable WANTED_CURL_ADAPTER must be : "
        "requests_curl, curl_cffi, or pycurl_requests"
    )

CURL_ADAPTER = None

if not CURL_ADAPTER and (
    not WANTED_CURL_ADAPTER or WANTED_CURL_ADAPTER == "requests_curl"
):
    try:
        from requests_curl import CURLAdapter

        CURL_ADAPTER = "requests_curl"
    except ImportError:
        pass

if not CURL_ADAPTER and (not WANTED_CURL_ADAPTER or WANTED_CURL_ADAPTER == "curl_cffi"):
    try:
        from curl_cffi_adapter import CurlHTTPAdapter

        CURL_ADAPTER = "curl_cffi"
    except ImportError:
        pass

if not CURL_ADAPTER and (
    not WANTED_CURL_ADAPTER or WANTED_CURL_ADAPTER == "pycurl_requests"
):
    try:
        import pycurl
        from pycurl_requests.adapters import PyCurlHttpAdapter

        CURL_ADAPTER = "pycurl_requests"
    except ImportError:
        pass

if not CURL_ADAPTER:
    raise ImportError(
        "Please install either (pycurl + requests_curl), "
        "(curl_cffi + curl_cffi_adapter), or (pycurl + pycurl_requests)"
    )

__all__ = ["ImpersonateHTTPAdapter"]

LOGGER.debug("Using Curl adapter: %s", CURL_ADAPTER)

if CURL_ADAPTER == "requests_curl":

    class ImpersonateHTTPAdapter(CURLAdapter):  # noqa: F811
        """
        Custom Adapter class that will impersonate a specific browser signature
        """

        def __init__(self, *args, **kwargs):
            self._proxy_headers = kwargs.pop("proxy_headers", {})
            impersonate_as = kwargs.pop("impersonate_as", DEFAULT_BROWSER_SIGNATURE)
            environ["CURL_IMPERSONATE"] = impersonate_as
            super().__init__(*args, **kwargs)

        def send(self, request, **kwargs):
            hdr = request.headers
            if hdr:
                hdr["user-agent"] = SKIP_HEADER
                hdr["accept-encoding"] = SKIP_HEADER
            return super().send(request, **kwargs)


if CURL_ADAPTER == "pycurl_requests":

    class ImpersonateHTTPAdapter(PyCurlHttpAdapter):  # noqa: F811
        """
        Custom Adapter class that will impersonate a specific browser signature
        """

        def __init__(self, *args, **kwargs):
            self._proxy_headers = kwargs.pop("proxy_headers", {})
            curl = pycurl.Curl()
            kwargs.pop("max_retries", None)
            impersonate_as = kwargs.pop("impersonate_as", DEFAULT_BROWSER_SIGNATURE)
            environ["CURL_IMPERSONATE"] = impersonate_as
            super().__init__(curl, *args, **kwargs)

        def send(self, request, **kwargs):
            hdr = request.headers
            if hdr:
                hdr["user-agent"] = SKIP_HEADER
                hdr["accept-encoding"] = SKIP_HEADER
            return super().send(request, **kwargs)


if CURL_ADAPTER == "curl_cffi":

    class ImpersonateHTTPAdapter(CurlHTTPAdapter):  # noqa: F811
        """
        Custom Adapter class that will impersonate a specific browser signature
        """

        def __init__(self, *args, **kwargs):
            self._proxy_headers = kwargs.pop("proxy_headers", {})
            kwargs.setdefault("impersonate_as", DEFAULT_BROWSER_SIGNATURE)
            super().__init__(*args, **kwargs)

        def send(self, request, **kwargs):
            hdr = request.headers
            if hdr:
                hdr.pop("user-agent", None)
            return super().send(request, **kwargs)
